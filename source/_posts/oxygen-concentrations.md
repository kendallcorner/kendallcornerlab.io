---
title: Oxygen Concentrations for QRA
date: 2019-11-04 16:44:46
tags: oxygen enrichment, oxygen
---

Concentration thresholds for oxygen are somewhat scarce. Here is a short list.

For personel safety in an enclosed space, [US OSHA](https://www.osha.gov/laws-regs/regulations/standardnumber/1915/1915.12#1915.12(a)) does not allow employees to enter enclosed spaces with greater than 22% oxygen.

For potential fatality, probits are generally used, however the National Insitute of Public Health and the Environment (the Netherlands) states that "An effective probit relationship cannot be worked out for oxygen" and instead suggests these concentrations for modeling potential fatalities direclty caused by oxygen exposure.

| Probability of Fatality | Oxygen Concentration |
| ------------------- | --- |
| $P_{lethal} = 0.1$  | greater than 40 vol% |
| $P_{lethal} = 0.01$ | between 30 and 40 vol% |
| $P_{lethal} = 0$    | between 20 and 30 vol% |

These percentages are including the oxygen already in air, however, most modeling softwares do not take into account that air is 20% oxygen, so some calculations will need to be done to properly model these concentrations.


## Oxygen percentage equations

These equations will help you calculate the concentration of oxygen you need to model to.

<br>
<center>$1$ volume of atmosphere $= 0.781$ $N_2 + 0.209$ $O_2 + 0.01$ other </center>
<br>

Adding a percentage of pure oxygen ($\alpha$) would push out some of the atmosphere leaving a percentage behind ($\beta$).

<br>
<center>$1$ volume of atmosphere $= \alpha$ $O_2 + \beta \cdot (0.781$ $N_2 + 0.209$ $O_2 + 0.01$ other)</center>
<br>

Balancing this equation on Oxygen only, $x$ is the total oxygen in the volume (concentration of interest):
$$ x = \alpha + \beta \cdot 0.209$$
$\alpha$ is the percentage of added oxygen and thus the concentration to be modeled. Using $\alpha + \beta = 1$ :
$$\alpha = ( x - 0.209)/(1-0.209)$$


<hr>

## Tool for calculating enrichment concentration

This tool does the above calculation to determine what percentage needs to be tracked in the modeling software
{% raw %}
<label>Oxygen Threshold: 
<input type="text" size="4" id="concentration-of-interest" name="concentration-of-interest" value="30">
 %</label>
<div id="answer">Concentration to track in modeling: 11.5%</div>
<script>
    const concentrationOfInterest = document.querySelector("#concentration-of-interest");
    concentrationOfInterest.oninput = () => {
        const trackingConcentration = Math.round((concentrationOfInterest.value - 20.9)/(1-0.209)*10)/10;
        answer = document.getElementById("answer");
        if (trackingConcentration < 0) { 
            answer.innerHTML = "Input is lower than Oxygen percentage in air";
        } else if (trackingConcentration > 100){
            answer.innerHTML = "Input is too high";
        } else { 
            answer.innerHTML = "Concentration to track in modeling:" + trackingConcentration.toString() + "%";
        }
    };
</script>
{% endraw %}

<hr>

{% blockquote %}
## References
1. Confined and Enclosed Spaces and Other Dangerous Atmospheres in Shipyard Employment, 29 CFR, § 1915.12(a) (1995).
2. Reference Manual Bevi Risk Assessments (2009). National Institute of Public Health and the Environment (RIVM). Bilthoven, the Netherlands

{% endblockquote %}