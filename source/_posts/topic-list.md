---
title: A list of topics
date: 2019-09-05 11:41:46
---

## Topics to cover:

The following is a list of topics that this site will attempt to cover. This may become a table of contents

### Formulas for modeling consequence assessment:
1. Explosions:
    - {% post_link BST-model %}
    - {% post_link TNO-model %}
    - TNT Model
1. Fires
    - jet/torch
    - pool
    -{% post_link api-fire-model %}
        * including recommended values and range tables
    - NFPA Method
1. BLEVE
5. Flash fire
6. Release Models
    - Max flow through pipe
    - Max flow through leak

### Consequences
1. Thresholds
    - Fire Radiation
    - Probits
    - Toxic materials
    - {% post_link oxygen-concentrations %}
    - Oxygen Deficiency

### PSV formulas:
1. API PSV formulas
2. PSV information including scenarios
    - Fire Zones for simultaneous relief
3. ASME requirements
4. API requirements
5. other requirements

### Siting: 
1. Spacing guideline information
2. NFPA
3. Electrical Area Classificiation

### Regulations:
1. OSHA PSM
2. EPA RMP

### Incident: 
1. PSE classifications according to API 754

### Risk
1. Tolerance Criteria, including MIACC, Purple Book, UK, etc.

### HAZOP
1. HAZOP methodology
1. HAZOP deviations
