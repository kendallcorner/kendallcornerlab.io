---
title: TNO-model
date: 2019-11-22 22:07:19
tags:
---

{% iframe "https://ryskconsulting.com/Plot2.html" 800 600 %}  

Scaled Overpressure:
$$p_s = p/p_a $$
<table class="equation-def" style="td{font-size: 0.8em}">
    <tr>
        <td>
            $p$
        </td>
        <td>Overpressure (Pa)
        </td>
    </tr>
    <tr>
        <td>
            $p_a$
        </td>
        <td>Atmospheric Pressure (Pa)
        </td>
    </tr>
 </table>  

Sachs Scaled Distance:
$$\bar{R} = R /(h_{combustion} \cdot V/p_a)^{1/3} $$
<table class="equation-def" style="td{font-size: 0.8em}">
    <tr>
        <td>
            $R$
        </td>
        <td>Radius ($m$)
        </td>
    </tr>
    <tr>
        <td>
            $h_{combustion}$
        </td>
        <td>Heat of combustion of stoichiometric hydrocarbon-air mixture ($J/m^3$)
        </td>
    </tr>
    <tr>
        <td>$V$
        </td>
        <td>volume of congested area (or in the case of DDT entire flammable cloud volume) ($m^3$)
        </td>
    </tr>
</table>

<hr>

## Tool for calculating TNO overpressure

This tool does the above calculation to determine overpressure based on radius and chosen TNO curve
{% raw %}
<label>Volume of Congested Area ($m^3$): 
<input type="text" class="TNO-input" size="4" id="volume" name="volume" value="10000">
 </label>
 <br><label>Distance from Explosion center ($m$): 
<input type="text" class="TNO-input" size="4" id="distance" name="distance" value="85">
 </label>
<div>Sachs Scaled Radius ($\bar{R}$): <span id="r-bar">10</span></div>
<p>Find the R value on the x axis and find the corresponding $p_s$ value on your chosen TNO curve, and input here. </p>
 <label>Scaled Overpressure ($p_s$): 
<input type="text" class="TNO-input" size="4" id="scaled-p" name="scaled-p" value=".09">
 </label>
 <div>Overpressure at distance: <span id="overpressure">10</span>psi</div>
<script>
    const tnoInputs = document.getElementsByClassName("TNO-input");

    function changeOutputs () {
        const volume = document.querySelector("#volume");
        const scaledP = document.querySelector("#scaled-p");
        const distance = document.querySelector("#distance");
        const rbar = Math.round((distance.value/(3500000 * volume.value / 101325 )**(1/3))*10)/10;
        // 3.5 x 10^6 J/m^3 and 101325 Pa
        document.querySelector("#r-bar").innerHTML = rbar.toString(); 
        const ovp = Math.round((scaledP.value*14.7)*10)/10; //14.7 psi
        document.querySelector("#overpressure").innerHTML = ovp.toString();

    }
    for (const element of tnoInputs) {
        element.addEventListener("input", changeOutputs);
    }

</script>
{% endraw %}

<hr>


{% blockquote %}
## References
1. TNO Yellow Book ... etc.

{% endblockquote %}