---
title: Baker-Strehlow-Tang Explosion Model
date: 2019-09-17 09:42:12
tags: explosion, BST
---

{% iframe "https://ryskconsulting.com/Plot1.html" 800 600 %}  

Scaled Overpressure:
$$p_s = p/p_a $$
<table class="equation-def" style="td{font-size: 0.8em}">
    <tr>
        <td>
            $p$
        </td>
        <td>Overpressure (Pa)
        </td>
    </tr>
    <tr>
        <td>
            $p_a$
        </td>
        <td>Atmospheric Pressure (Pa)
        </td>
    </tr>
 </table>  

Sachs Scaled Distance:
$$\bar{R} = R /(h_{combustion} \cdot V/p_a)^{1/3} $$
<table class="equation-def" style="td{font-size: 0.8em}">
    <tr>
        <td>
            $R$
        </td>
        <td>Radius ($m$)
        </td>
    </tr>
    <tr>
        <td>
            $h_{combustion}$
        </td>
        <td>Heat of combustion of stoichiometric hydrocarbon-air mixture ($J/m^3$)
        </td>
    </tr>
    <tr>
        <td>$V$
        </td>
        <td>volume of congested area (or in the case of DDT entire flammable cloud volume) ($m^3$)
        </td>
    </tr>
</table>

<table class="data-table">
    <thead>
        <tr>
            <th rowspan=2 colspan=2>2D Flame Expansion</th>
            <th colspan=3>Obstacle Density</th>
        </tr>
        <tr>
            <th>High</th>
            <th>Medium</th>
            <th>Low</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th rowspan=3>Reactivity</th>
            <th>High</th>
            <td>DDT</td>
            <td>DDT</td>
            <td>0.59</td>
        </tr>
        <tr>
            <th>Medium</th>
            <td>1.6</td>
            <td>0.66</td>
            <td>0.47</td>
        </tr>
        <tr>
            <th>Low</th>
            <td>0.66</td>
            <td>0.47</td>
            <td>0.079</td>
        </tr>
    </tbody>
</table>

<table class="data-table">
    <thead>
        <tr>
            <th rowspan=2 colspan=2>2.5D Flame Expansion</th>
            <th colspan=3>Obstacle Density</th>
        </tr>
        <tr>
            <th>High</th>
            <th>Medium</th>
            <th>Low</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th rowspan=3>Reactivity</th>
            <th>High</th>
            <td>DDT</td>
            <td>DDT</td>
            <td>0.47</td>
        </tr>
        <tr>
            <th>Medium</th>
            <td>1.0</td>
            <td>0.55</td>
            <td>0.29</td>
        </tr>
        <tr>
            <th>Low</th>
            <td>0.50</td>
            <td>0.35</td>
            <td>0.053</td>
        </tr>
    </tbody>
</table>

<table class="data-table">
    <thead>
        <tr>
            <th rowspan=2 colspan=2>3D Flame Expansion</th>
            <th colspan=3>Obstacle Density</th>
        </tr>
        <tr>
            <th>High</th>
            <th>Medium</th>
            <th>Low</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th rowspan=3>Reactivity</th>
            <th>High</th>
            <td>DDT</td>
            <td>DDT</td>
            <td>0.36</td>
        </tr>
        <tr>
            <th>Medium</th>
            <td>0.50</td>
            <td>0.44</td>
            <td>0.11</td>
        </tr>
        <tr>
            <th>Low</th>
            <td>0.34</td>
            <td>0.23</td>
            <td>0.026</td>
        </tr>
    </tbody>
</table>


{% blockquote %}
## References
1. Baker, Q.A., M.J. Tang, et al. (1994), _Vapor Cloud Explosion Analysis_, 28th Loss Prevention Symposium, Atlanta, GA. American Institute of Chemical Engineers.

{% endblockquote %}