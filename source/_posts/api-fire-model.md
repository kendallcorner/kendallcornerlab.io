---
title: API 521 Method for Fire Evaluation
date: 2019-09-11 16:49:46
tags: fire, modeling, API
---

API 521 has provided a fire radiation model that is often used in flare and other types of fire modeling

$$ q_{absorbent} = \sigma(\alpha_{surface} \cdot \varepsilon_{fire} \cdot T^4_{fire} - \varepsilon_{surface} - T^4_{surface}) + h(T_{gas} - T_{surface}) $$

<table class="equation-def" style="td{font-size: 0.8em}">
    <tr>
        <td>
            $q_{absorbent}$
        </td>
        <td>absorbed heat flux from the fire ($Wm^{-2}$ or $Btu \text{ } h^{-1}ft^{-2}$)
        </td>
    </tr>
    <tr>
        <td>
            $\sigma$
        </td>
        <td>
            Stefan-Blotzmann constant ($5.67\mathrm{e}{\text{-}8}$ &nbsp; $Wm^{-2}K^{-4}$ &nbsp; or &nbsp; $0.1713e{\text{-}8}$ &nbsp; $Btu\text{ }h^{-1}ft^{-2}{^{\circ}R}^{-4}$)
        </td>
    </tr>
    <tr>
        <td>
            $\alpha_{surface}$
        </td>
        <td>
            equipment absorptivity (dimensionless)
        </td>
    </tr>
    <tr>
        <td>
            $\varepsilon_{fire}$
        </td>
        <td>
            fire emissivity (dimensionless)
        </td>
    </tr>
    <tr>
        <td>
            $\varepsilon_{surface}$
        </td>
        <td>
            surface emissivity (dimensionless)
        </td>
    </tr>
    <tr>
        <td>
            $T_{fire}$
        </td>
        <td>
            fire temperature ($K$ or ${^{\circ}R}$)
        </td>
    </tr>
    <tr>
        <td>
            $T_{surface}$
        </td>
        <td>
            equipment temperature ($K$ or ${^{\circ}R}$)
        </td>
    </tr>
    <tr>
        <td>
            $h$
        </td>
        <td>
            convective heat transfer coefficient fo air/fire in contact with the equipment ($Wm^{-2}K^{-1}$ or $Btu\text{ }h^{-1}ft^{-2}{^{\circ}R}^{-1}$)
        </td>
    </tr>
    <tr>
        <td>
            $T_{gas}$
        </td>
        <td>
            temperature of air/fire in contact with equipment surface ($K$ or ${^{\circ}R}$)
        </td>
    </tr>
</table>

{% blockquote %}
## References
1. API STD 521 (2014), _Pressure Relieving and Depressuring Systems, 6th Edition_, American Petroleum Institute.

{% endblockquote %}