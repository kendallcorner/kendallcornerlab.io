---
title: math
tags:
---

---
title: mathTest
date: 2019-09-04 16:45:50
tags:
---


Test for Hexo Math

Simple inline $a = b + c$.

$$\frac{\partial u}{\partial t}
= h^2 \left( \frac{\partial^2 u}{\partial x^2} +
\frac{\partial^2 u}{\partial y^2} +
\frac{\partial^2 u}{\partial z^2}\right)$$

Stefan-Blotzmann constant ($5.67\mathrm{e}{\text{-}8}$ &nbsp; $Wm^{-2}K^{-4}$ &nbsp; or &nbsp; $0.1713e{\text{-}8}$ &nbsp; $Btu\text{ }h^{-1}ft^{-2}{^{\circ}R}^{-4}$)

Stefan-Blotzmann constant ($5.67\mathrm{e}{\text{-}8}$ &nbsp; $W\cdot m^{-2}\cdot K^{-4}$ &nbsp; or &nbsp; $0.1713e{\text{-}8}$ &nbsp; $Btu\text{ }h^{-1}ft^{-2}{^{\circ}R}^{-4}$)

