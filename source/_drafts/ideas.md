---
title: ideas
tags:
---


##Topics to Consider:

1. Equations for predicting LFL from Crowl & Cowan?
2. NFPA 68 Flame speeds
3. Dow Chemical Fire and Explosion Index and Chemical Exposure Index
4. "Flare Radiation Heat Tables" (Pool and Jet fire radiant heat curves for common materials)
5. "Overpressure congestion curves"
6. Acronym definitions
7. Definitions of NFPA/US Gov't flammable, combustible, ignitable, etc. and classes